import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EngineModel } from '../model/engine.model';

@Injectable({
  providedIn: 'root'
})
export class EngineService {

  prefix = 'http://localhost:8080/';

  constructor(
    private http: HttpClient
  ) { }

  // CREATE
  create(engine: EngineModel) {
    return this.http.post<any>(this.prefix + 'api/v1/engine', engine);
  }

  // READ - GETALL
  getAll() {
    return this.http.get<any>(this.prefix + 'api/v1/engine/all');
  }
  
  // UPDATE
  update(engine: EngineModel) {
    return this.http.put(this.prefix + 'api/v1/engine/', engine);
  }

  // DELETE
  delete(id: number) {
    return this.http.delete(this.prefix + 'api/v1/engine/' + id);
  }
}
