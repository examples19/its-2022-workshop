import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { EngineModel } from '../model/engine.model';

@Component({
  selector: 'app-engine-form',
  templateUrl: './engine-form.component.html',
  styleUrls: ['./engine-form.component.css']
})
export class EngineFormComponent implements OnInit, OnChanges {

  @Output() formSubmitted = new EventEmitter();
  @Input() engine: EngineModel = new EngineModel();

  constructor(
    private fb: FormBuilder
  ) { }

  engineForm = this.fb.group({
    id: [null],
    name: [null, Validators.required],
    description: [null, Validators.required],
    fuelType: [null, Validators.required],
    price: [null, Validators.required]
  });

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
    if ( changes['engine'].currentValue ) {
      if (changes['engine'].currentValue.id != null) {
        this.engineForm.patchValue(changes['engine'].currentValue);
      }
      else {
        this.engineForm.reset();
      }
    }
  }

  formSubmit() {
    console.log("FORM SUBMITTED", this.engineForm.value);
    this.formSubmitted.emit(this.engineForm.value);
  }

}
