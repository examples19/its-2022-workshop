import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { EngineModel } from '../model/engine.model';
import { EngineService } from '../service/engine.service';

@Component({
  selector: 'app-engine',
  templateUrl: './engine.component.html',
  styleUrls: ['./engine.component.css']
})
export class EngineComponent implements OnInit {

  engineList: EngineModel[] = [];
  displayDialog = false;
  selectedEngine: EngineModel = new EngineModel();

  constructor(
    private engineService: EngineService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit(): void {
    this.getAllEngine();
  }

  getAllEngine() {
    this.engineService.getAll().subscribe(response => {
      console.log(response);
      this.engineList = response;
    });
  }

  addNewEngine() {
    this.selectedEngine = new EngineModel();
    this.displayDialog = true;
  }

  formSubmitted(engine: any) {
    this.displayDialog = false;

    console.log("EVENT RECEIVED", engine);
    
    if (engine.id == null) {
      this.engineService.create(engine).subscribe(result => {
        console.log("RESULT OF THE POST", result);
        //this.getAllEngine();
        this.engineList.push(result);
        this.messageService.add({
          severity:'success', 
          summary:'Engine created', 
          detail:'OK'
        });
      });
    } else if (engine.id != null) {
      this.engineService.update(engine).subscribe(result => {
        console.log("RESULT OF THE PUT", result);
        this.getAllEngine();
        this.messageService.add({
          severity:'success', 
          summary:'Engine updated', 
          detail:'OK'
        });
      });
    }
  }

  onEngineClicked(engine: EngineModel) {
    this.displayDialog = true;
    this.selectedEngine = engine;
  }

  delete($event: any, engine: EngineModel) {
    $event.stopPropagation();

    this.confirmationService.confirm({
      message: 'Sei sicuro di voler cancellare questo motore?',
      accept: () => {
        if(engine.id) {
          this.engineService.delete(engine.id).subscribe(result => {
            this.getAllEngine();
            this.messageService.add({
              severity:'success', 
              summary:'Engine deleted', 
              detail:'OK'
            });
          })
        }   
      }
    });    
  }
}
