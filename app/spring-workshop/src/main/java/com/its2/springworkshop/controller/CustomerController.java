package com.its2.springworkshop.controller;

import com.its2.springworkshop.entity.CustomerEntity;
import com.its2.springworkshop.entity.EngineEntity;
import com.its2.springworkshop.service.CustomerService;
import com.its2.springworkshop.service.EngineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api/v1/customer")
public class CustomerController {

    // CRUD = Create, Read, Update, Delete
    // Dependency injection
    @Autowired
    CustomerService service;

    @GetMapping( value="/all" )
    public ResponseEntity<?> getAll() {
        return new ResponseEntity<>(service.getAll(), HttpStatus.ACCEPTED);
    }

    @GetMapping( value="/{id}" )
    public ResponseEntity<?> getById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(service.getById(id), HttpStatus.ACCEPTED);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody CustomerEntity entity) {
        return new ResponseEntity<>(service.create(entity), HttpStatus.CREATED);
    }

    @DeleteMapping(value = {"/{id}"})
    public ResponseEntity<?> delete(@PathVariable("id") int id) {
        try {
            service.delete(id);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<?> deleteByRequestParam(@RequestParam("id") int id) {
        try {
            service.delete(id);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody CustomerEntity entity) {
        CustomerEntity response = new CustomerEntity();
        try {
            response = service.update(entity);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
