package com.its2.springworkshop.controller;

import com.its2.springworkshop.entity.EngineEntity;
import com.its2.springworkshop.service.EngineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api/v1/engine")
@CrossOrigin( origins = "*", maxAge = 3600 )
public class EngineController {

    // CRUD = Create, Read, Update, Delete
    // Dependency injection
    @Autowired
    EngineService service;

    @GetMapping( value="/all" )
    public ResponseEntity<?> getAll() {
        return new ResponseEntity<>(service.getAll(), HttpStatus.ACCEPTED);
    }

    // Parameter on path (api/v1/engine/xx)
    @GetMapping( value="/{id}" )
    public ResponseEntity<?> getById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(service.getById(id), HttpStatus.ACCEPTED);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    // Parameter on query (?id=xx)
    @GetMapping
    public ResponseEntity<?> getByIdWithParam(@RequestParam("id") int id) {
        try {
            return new ResponseEntity<>(service.getById(id), HttpStatus.ACCEPTED);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody EngineEntity engineToCreate) {
        return new ResponseEntity<>(service.create(engineToCreate), HttpStatus.CREATED);
    }

    @DeleteMapping(value = {"/{id}"})
    public ResponseEntity<?> delete(@PathVariable("id") int id) {
        try {
            service.delete(id);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<?> deleteByRequestParam(@RequestParam("id") int id) {
        try {
            service.delete(id);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(value="/all")
    public ResponseEntity<?> deleteAll() {
        try {
            service.deleteAll();
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody EngineEntity entityToUpdate) {
        EngineEntity response = new EngineEntity();
        try {
            response = service.update(entityToUpdate);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
