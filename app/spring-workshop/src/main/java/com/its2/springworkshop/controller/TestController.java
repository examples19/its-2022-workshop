package com.its2.springworkshop.controller;

import com.its2.springworkshop.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping( value="api/v1/test" )
public class TestController {

    // Dependency injection
    @Autowired
    TestService testService;

    @GetMapping( value="/{name}" )
    public ResponseEntity<?> sayHello(@PathVariable("name") String name) {
        return new ResponseEntity<>("Benvenuto " + name, HttpStatus.ACCEPTED);
    }

    @GetMapping( value="sum/{n1}/{n2}")
    public ResponseEntity<?> doSum(@PathVariable("n1") int n1, @PathVariable("n2") int n2) {
        return new ResponseEntity<>("La somma è: " + testService.doSum(n1, n2), HttpStatus.ACCEPTED);
    }
}
