package com.its2.springworkshop.repository;

import com.its2.springworkshop.entity.EngineEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EngineRepository extends JpaRepository<EngineEntity, Integer> {
}
