package com.its2.springworkshop.service;

import com.its2.springworkshop.entity.CustomerEntity;
import com.its2.springworkshop.entity.EngineEntity;
import com.its2.springworkshop.repository.CustomerRepository;
import com.its2.springworkshop.repository.EngineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {
    @Autowired
    CustomerRepository repository;

    public List<CustomerEntity> getAll() {
        return repository.findAll();
    }

    public Optional<CustomerEntity> getById(int id){
        return Optional.ofNullable
                (repository.findById(id)
                        .orElseThrow(() -> new EntityNotFoundException("Entity not found")));
    }

    public CustomerEntity create(CustomerEntity entity) {
        return repository.save(entity);
    }

    public void delete(int id) {
        CustomerEntity entity = repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Entity not found"));
        //EngineEntity entity = getById(id).get();
        repository.delete(entity);
    }

    public void deleteAll() {
        repository.findAll().forEach(e -> repository.delete(e));
    }

    public CustomerEntity update(CustomerEntity entityToUpdate) {
        repository.findById(entityToUpdate.getId())
            .orElseThrow(() -> new EntityNotFoundException("Entity not found"));
        return repository.save(entityToUpdate);
    }
}
