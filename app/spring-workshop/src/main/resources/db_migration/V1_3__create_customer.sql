Create table if not exists t_customer(
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(30) DEFAULT NULL,
    `surname` varchar(255) DEFAULT NULL,
    `nationality` varchar(255) DEFAULT NULL,
    CONSTRAINT customer_pk PRIMARY KEY (id)
    );
