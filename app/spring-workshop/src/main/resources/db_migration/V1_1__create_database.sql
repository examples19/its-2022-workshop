Create table if not exists t_engine(
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(30) DEFAULT NULL,
    `description` varchar(255) DEFAULT NULL,
    `fuel_type` varchar(20) DEFAULT NULL,
    `price` int(10) DEFAULT 0,
    CONSTRAINT engine_pk PRIMARY KEY (id)
    );
