## STEP LIST

---
**NOTE**

All the steps regarding the application, was committed with a separate message.
Used presentation in **resources** folder.

---

1. Java + IntelliJ + Postman installation
2. Springboot initialization --> [Spring initializer](start.spring.io)
3. Controller creation
4. Controller test
5. Service creation
6. Service test
7. Dependencies added on project (on pom.xml):
  - spring-boot-starter-data-jpa
  - h2
  - lombok
  - flyway-core
8. Flyway migration file
9. Entity creation (Engine)
10. Repository creation (Engine)
11. Service creation (Engine)
12. Controller creation (Engine)
13. Full CRUD, controller and service (Engine)
14. Angular project creation
15. HomeComponent creation
16. EngineComponent creation
17. Service creation for API consumption
18. Service test
19. PrimeNg installation
20. PrimeNg components usage
21. Full API usage for ENGINE. Response rendered on different PrimeNg components.

## NOT INCLUDED IN THE PREVIOUS

### Build Angular project
1. Go to angular project folder
2. Build Angular project: `ng build`
3. The result of the compilation will be in `dist` folder

![NG Build result](resources/img/ng-build-result.PNG)

### Serve the compiled Angular project using Tomcat server exposed by Spring Boot (BE serving FE)
This step, will bring to the possibility of creating a standalone application, with the FE and BE served by the same service.

1. Copy the files result of the build, inside Spring Boot project, under `static` folder. The result has to be as it follows.

![Static folder](resources/img/static-folder.PNG)

2. Running the Spring Boot application, will render the files on port used by Tomcat.

![Application served](resources/img/app-served.PNG)

### Run the entire Web App (BE serving the FE) as Docker Container
This step will bring the creation of a container of a Java application, running it inside Docker.
The following commands refers to the previous step (compiled frontend moved in `static` folder).

1. Create package (JAR) of the project (including FE in static folder, as previous).

![Package creation](resources/img/package-creation.PNG)

  - This will create a fat (with all dependencies) JAR file in `target` folder

  ![Created JAR](resources/img/created-jar.PNG)

2. On the folder that contains `Dockerfile` file, run: `docker build -t its-workshop .` in order to create the container.
3. Run the container with `docker run --publish 8080:8080 --name its-workshop its-workshop`

  ![Run Container](resources/img/run-container.PNG)
