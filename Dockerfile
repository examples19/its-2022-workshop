# syntax=docker/dockerfile:1
FROM openjdk:11.0.12-jre-slim-buster
ARG JAR_FILE=app/spring-workshop/target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar", "/app.jar"]
